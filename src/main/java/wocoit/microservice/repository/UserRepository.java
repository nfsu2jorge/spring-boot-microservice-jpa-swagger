package wocoit.microservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import wocoit.microservice.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

}
